# Controller for managing post versions and differences
class VersionsController < ApplicationController
  before_action :set_post_and_version, only: %i(diff rollback destroy)

  def diff; end

  def rollback
    # change the current document to the specified version
    # reify gives you the object of this version
    post = @version.reify
    post.save
    redirect_to edit_post_path(post)
  end

  private

  def set_post_and_version
    @post = Post.find(params[:post_id])
    @version = @post.versions.find(params[:id])
  end
end
