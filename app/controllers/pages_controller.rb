# Controller for different pages
class PagesController < ApplicationController
  def about
    @title = 'About us'
    @content = 'Welcome to training manager. Here you can log and and
    analyze your training results'
  end
end
