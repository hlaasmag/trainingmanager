# Model for trainingposts. Validation also takes place here.
class Post < ApplicationRecord
  has_paper_trail
  validates :title, presence: true,
                    length: { minimum: 4 }
end

# Currently used for activating paper trail gem
class Widget < ActiveRecord::Base
  has_paper_trail
  # has_paper_trail on: [:update, :create]
end
