# README

This is a webpage where you can add your training posts and view them later.

Currently you can add, edit, view posts and delete and navigate in the page.

There is also a comprehensive version history when one is in edit mode. This
allows to view differences between current and previous version and to rollback
to a previous version.

* Ruby version -  ruby 2.3.3p222 (2016-11-21 revision 56859)

* Rails version - Rails 5.0.1


I set up the tools in Linux from scratch with this guide:
http://installfest.railsbridge.org/installfest/

If one has ruby, one might only be able to start the project by only
downloading the project,typing to terminal:
gem install bundler

if rails is installed, whilst being at the root of the project one has to
issue command in terminal which starts the server which needs to be running
to view the localhost:
'rails s'

With the server running you can view the page by typing
'http://localhost:3000/' to the browser. (3000 should be default)
